'use strict';

var repl = require('quick-repl');
var url = require('minimist')(process.argv).url;

if (!url) {
  console.log('missing --url [url]');
  process.exit(-1);
} else {
  var ClientIO = require('origami2-socket.io').ClientIO;
  var TokenProviderIO = require('origami2-socket.io').TokenProviderIO;
  var clientIo = new ClientIO(require('origami2-core').CryptoUtils.randomKey(512));
  var tokenProviderIo = new ClientIO(require('origami2-core').CryptoUtils.randomKey(512));

  clientIo
  .connect(url)
  .then(
    function (client) {
      // promisify(
        repl.start(
          function(err, context) {
           if (err) throw err;
        
           for (var a in client) {
             context[a] = client[a];
           }
          }
        )
      // );
    }
  );
}
